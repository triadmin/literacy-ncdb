<?php
defined('C5_EXECUTE') or die("Access Denied.");
$this->inc('elements/header.php'); ?>
    <div data-role="panel" data-id="menu" data-hash="crumbs" data-context="a#default">

      <div data-role="page" id="main">
        <div data-role="header" data-backbtn="false" >
          <h1>Menu</h1>
        </div>

        <div data-role="content">
          <?php
			$as = new Area('Sidebar');
			$as->display($c);
			?>	 
        </div>
      </div><!-- /page -->

    </div><!-- panel menu -->

    <div data-role="panel" data-id="main">

      <div data-role="page" id="content">
        <div data-role="header">
          <h1>About SplitView</h1>
        </div><!-- /header -->

        <div data-role="content">
          <?php
			$a = new Area('Main');
			$a->display($c);
			?>
        </div><!-- /content -->

        <div data-role="footer" data-position="fixed" data-id="ew-footer" class="ui-splitview-hidden">
          <div data-role="navbar">
            <ul>
              <li><a href="#main" class="ui-btn-active" data-transition="slideup">Menu</a></li>
              <li><a href="#resources" data-transition="slideup">Resources</a></li>
            </ul>
          </div><!-- /navbar -->
          <h2>Hi</h2>
        </div><!-- /footer -->
      </div><!-- /page -->

    </div><!-- panel main -->
<? $this->inc('elements/footer.php'); ?>