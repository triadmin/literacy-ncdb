<!DOCTYPE html>
<html>
	<head>
	<title><?php echo SITE; ?></title>
    <meta name="viewport" content="width=device-width, minimum-scale=1, maximum-scale=1">

    <?php  Loader::element('header_required'); ?>

    <!-- Site Header Content //-->
    <link rel="stylesheet" href="<?=$this->getThemePath(); ?>/css/jquery.mobile-1.0b1.css" />
	<link rel="stylesheet" href="<?=$this->getThemePath(); ?>/css/jquery.mobile.splitview.css" />
    <link rel="stylesheet" href="<?=$this->getThemePath(); ?>/css/jquery.mobile.scrollview.css" />
</head>
<body>