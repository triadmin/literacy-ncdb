<!-- Oct 29: Replace GA tracking code -->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-1427386-7', 'auto');
  ga('send', 'pageview');

</script>

<!-- July 18 2016: Add Monsido tracking code -->
<script type="text/javascript">
  var _monsido = _monsido || [];
  _monsido.push(['_setDomainToken', 'XgeNPB9vNQZVxs21XNW0rQ']);
  _monsido.push(['_withStatistics', 'true']);
</script>
<script src="//cdn.monsido.com/tool/javascripts/monsido.js"></script>
<?php  Loader::element('footer_required'); ?>
</body>
</html>
