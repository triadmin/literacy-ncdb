<!DOCTYPE html>
<html lang="en">
	<head>
				<?php Loader::element('header_required'); ?>
        <meta http-equiv="x-ua-compatible" content="ie=edge" />
        <meta name="apple-mobile-web-app-capable" content="yes" />
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" />
        <LINK REL="icon" HREF="<?=$this->getThemePath(); ?>/images/favicon.ico" TYPE="image/ico" />
        <LINK REL="shortcut icon" HREF="<?=$this->getThemePath(); ?>/images/favicon.ico" TYPE="image/ico" />

        <link rel="stylesheet" href="<?=$this->getThemePath(); ?>/css/html.css" />
        <link rel="stylesheet" href="<?=$this->getThemePath(); ?>/css/text.css" />
        <link rel="stylesheet" href="<?=$this->getThemePath(); ?>/css/form.css" />
        <link rel="stylesheet" href="<?=$this->getThemePath(); ?>/css/icons.css" />
        <link rel="stylesheet" href="<?=$this->getThemePath(); ?>/css/site.css" />
        <link rel="stylesheet" href="<?=$this->getThemePath(); ?>/css/custom.css" />
  
        <!--[if IE 8]>
        <link rel="stylesheet" href="<?=$this->getThemePath(); ?>/css/ie8.css" />
        <![endif]-->
        <!--[if !IE]><!-->
        <!--<script src="<?=$this->getThemePath(); ?>/js/iscroll.js"></script>-->
        <!--<![endif]-->
        <script src="<?=$this->getThemePath(); ?>/js/jquery.qtip-1.0.0-rc3.min.js"></script>
        <script src="<?=$this->getThemePath(); ?>/js/fadeSlideShow.js"></script>
        <script src="<?=$this->getThemePath(); ?>/js/master.js"></script>
</head>
<body>
