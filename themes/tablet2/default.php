<?php
defined('C5_EXECUTE') or die("Access Denied.");
$this->inc('elements/header.php');

$page = Page::getByID($c->getCollectionParentID());
$parent_url = View::URL($page->getCollectionPath());
$nh=Loader::helper('navigation');
$breadcrumb = $nh->getTrailToCollection($c);
?>
<div id="main" class="abs">
	<div class="abs header_upper chrome_brown">
		<span class="float_left button-orange" id="button_navigation">Main Menu</span>
        <!-- If you want header buttons, this is how you do it. -->
        <? if (count($breadcrumb)>0) { ?>
		<button id="buttonBack" style="margin-left: 9px;" class="float_left button-blue">
			Back
		</button>
        <? } ?>
        <a href="http://literacy.nationaldb.org/index.php/contact" class="float_left button-orange">Contact Us</a>
        <div class="siteLogo" style="margin-top: 1px;">Literacy for Children with<br />Combined Vision and Hearing Loss</div>
        <div class="siteLogoRight"></div>
		<!--<div class="newsBox"><a href="http://www.nationaldb.org/ISFutureWebinars.php?rID=2012-0013">NCDB Literacy Site Webinar</a> Feb 16, 10 a.m. PST</div>-->
    <div style="float: right; margin: 11px 30px 0 0;">
      <div id="embed_launch_pad"> </div>
      <script type="text/javascript" src="https://nationaldb.org/assets/js/widgets/widget_launchpad.js" data-site="literacy" data-align="bottom-left"></script>
    </div>
        <div style="clear: both;"></div>
	</div>
    <!-- This is another header bar -->
	<!-- <div class="abs header_lower chrome_dark"></div> -->

	<div id="main_content" class="abs">
		<div id="main_content_inner" style="margin-bottom: 150px;">
            <?php
			$a = new Area('Main');
			$a->display($c);
			?>
		</div>
	</div>
	<div class="abs footer_lower chrome_brown">
        <!-- This is how you would do icons and buttons in the footer bar -->
		<!--
		<a href="#" class="float_left button">
			Bar
		</a>
		<a href="#" class="icon icon_bird float_right"></a>
		<a href="#">View full site</a>
		-->
        <p style="float: left; margin: 6px 0 0 12px; width: 98%;" class="subText">
            Funded through award #H326T130013 by the Office of Special Education Programs, U.S. Department of Education.
			The opinions and policies expressed by this publication do not necessarily reflect those of The Teaching
			Research Institute, Western Oregon University, or the U.S. Department of Education.
        </p>
        <div style="clear: left;"></div>
	</div>
</div>
<!-- Main Menu Sidebar -->
<div id="sidebar" class="abs">
	<span id="nav_arrow"></span>
    <div class="abs header_upper chrome_brown">
        <form action="/index.php/search-results" method="get" style="margin-top: 12px;">
            <input name="search_paths[]" type="hidden" value="" />
            <input type="text" name="query" placeholder="Search this site" size="30" />
        </form>
    </div>
	<div id="sidebar_content" class="abs">
		<div id="sidebar_content_inner">
            <?php
			$as = new Area('Sidebar');
			$as->display($c);
			?>
		</div>
	</div>

	<div class="abs footer_lower chrome_brown">
        <!--
        If you want a footer in the menu area, this is how you do it
		<a href="#" class="icon icon_gear2 float_left"></a>
		<span class="float_right gutter_right">Some descriptive text here</span>
        -->
		<a href="http://www.osepideasthatwork.org/" title="OSEP Ideas that Work" id="logoContainerIDEA"></a>
        <!--<a href="http://www.tadnet.org" title="TA&D Network" id="logoContainerTAD"></a>-->
		<a href="http://www.nationaldb.org" title="NCDB" id="logoContainerNCDB"></a>
	</div>
</div>
<? $this->inc('elements/footer.php'); ?>
