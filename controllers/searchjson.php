<?
 
class SearchjsonController extends Controller {
  public function do_search() {
		$q = $_REQUEST['query'];
		$_q = $q;
		Loader::library('database_indexed_search');
		$ipl = new IndexedPageList();
		$aksearch = false;
		if (is_array($_REQUEST['akID'])) {
			Loader::model('attribute/categories/collection');
			foreach($_REQUEST['akID'] as $akID => $req) {
				$fak = CollectionAttributeKey::getByID($akID);
				if (is_object($fak)) {
					$type = $fak->getAttributeType();
					$cnt = $type->getController();
					$cnt->setAttributeKey($fak);
					$cnt->searchForm($ipl);
					$aksearch = true;
				}
			}
		}
		
		if (empty($_REQUEST['query']) && $aksearch == false) {
			return false;		
		}
		
		$ipl->setSimpleIndexMode(true);
		if (isset($_REQUEST['query'])) {
			$ipl->filterByKeywords($_q);
		}
		
		if( is_array($_REQUEST['search_paths']) ){ 
			
			foreach($_REQUEST['search_paths'] as $path) {
				if(!strlen($path)) continue;
				$ipl->filterByPath($path);
			}
		}

		$res = $ipl->getPage(); 
		
		foreach($res as $r) { 
      $content = substr( $r['content'], 0, (250));
      $content = substr( $content, 0, strrpos( $content, ' ' )) . '...';
      $path = BASE_URL . $r['cPath'];
			$results[] = new IndexedSearchResult($r['cID'], $r['cName'], $r['cDescription'], $r['score'], $path, $content);
    }
    
    // Add some additional stuff to array
    foreach ($results as $r)
    {
      $r->site_name = 'Literacy';
      $r->site_url = BASE_URL;
      $r->site_icon_class = 'icon-literacy24';
    }  
    
    $jh = Loader::helper('json');
    echo $jh->encode($results);
    exit;
	}		
}
